fun main() {
    val obj = Point(1, 7)
    val obj2 = Point(5, 7)
    val obj3 = Point(1, 7)

    println(obj == obj2)
    println(obj == obj3)
    obj.moveSymmetrically()

}

data class Point(var x: Int, var y: Int) {
    override fun toString(): String {
        return "Poits : X is $x Y is $y"
    }

    fun moveSymmetrically() {
        this.x = -x
        this.y = -y
    }

    fun equals(obj: Point): Boolean {
        return this.x == obj.x && this.y == obj.y
    }
}